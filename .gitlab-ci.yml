
################
# Set global defaults, including:
# - interruptible:true, so that new pushes can stop the current job and start a new one
# - php image
################

default:
  interruptible: true


################
# Stages
#
# Each job is assigned to a stage, defining the order in which the jobs are executed.
# Jobs in the same stage run in parallel.
#
################

stages:
  ################
  # Build
  #
  # In the Build stage we are assembling our test environment:
  #   * Selecting the core version to test against
  #   * Selecting php version
  #   * Selecting the database, and configuring it
  #   * Plus any additional build steps, like composer runs, etc
  # Wherever possible, we use variables defined in: include.druaplci.variables.yml so that the configuration can stay up to date with current Drupal Core development.
  #
  # Documentation: https://docs.gitlab.com/ee/ci/yaml/#stages
  ################
  - build

################
# Jobs
#
# Jobs define what scripts are actually executed in each stage.
#
# The 'rules' keyword can also be used to define conditions for each job.
# # @TODO: Use rules to define core version to test against, allow overriding default on manual run with variables. (Perhaps with 'variables in variables')
#
# Documentation: https://docs.gitlab.com/ee/ci/jobs/
################

################
# Build Jobs
################

# A hidden re-usable job. Useful when using a job matrix.
# For example https://git.drupalcode.org/project/keycdn
.docker-build:
  stage: build
  image: quay.io/buildah/stable
  variables:
    # Use vfs with buildah. Docker offers overlayfs as a default, but buildah
    # cannot stack overlayfs on top of another overlayfs filesystem.
    STORAGE_DRIVER: vfs
    # Write all image metadata in the docker format, not the standard OCI format.
    # Newer versions of docker can handle the OCI format, but older versions, like
    # the one shipped with Fedora 30, cannot handle the format.
    BUILDAH_FORMAT: docker
    # You may need this workaround for some errors: https://stackoverflow.com/a/70438141/1233435
    BUILDAH_ISOLATION: chroot
  rules:
    - when: always
  artifacts:
    expire_in: 1 week
    expose_as: 'built-environments'
    when: always
    exclude:
      - .git
      - .git/**/*
    paths:
      - .
  script:
    - ln -s buildah /usr/bin/docker
    - dnf install -y --nodocs git
    - git config --global --add safe.directory $(pwd)
    - echo "${DOCKER_HUB_PASSWORD}" | buildah login -u "${DOCKER_HUB_USER}" --password-stdin docker.io
    - echo 'Testing ./build_containers.sh'
    - chmod +x ./build_containers.sh; ./build_containers.sh $CI_COMMIT_BRANCH $DOCKER_HUB

docker:
  extends: .docker-build


################
# Workflow
#
# Rules in a job defininition define conditions for when the pipeline will run.
#   For example:
#     * On commit
#     * On merge request
#     * On manual trigger
#     * etc. 
# https://docs.gitlab.com/ee/ci/jobs/job_control.html#specify-when-jobs-run-with-rules 
# 
# Pipelines can also be configured to run on a schedule,though they still must meet the conditions defined in Workflow and Rules. This can be used, for example, to do nightly regression testing: 
# https://gitlab.com/help/ci/pipelines/schedules 
################

# @TODO: Still not sure what the best defaults should be for this template
workflow:
  rules:
    # Run on commits to the default & release branches.
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_ROOT_NAMESPACE == "project"
    # The last rule above blocks manual and scheduled pipelines on non-default branch. The rule below allows them:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_PROJECT_ROOT_NAMESPACE == "project"
    # Run if triggered from Web using 'Run Pipelines'
    - if: $CI_PIPELINE_SOURCE == "web"


###################################################################################
#
#                                        *                                      
#                                       /(                                      
#                                      ((((,                                    
#                                    /(((((((                                   
#                                   ((((((((((*                                 
#                                ,(((((((((((((((                               
#                              ,(((((((((((((((((((                             
#                            ((((((((((((((((((((((((*                          
#                         *(((((((((((((((((((((((((((((                        
#                       ((((((((((((((((((((((((((((((((((*                     
#                    *((((((((((((((((((  .((((((((((((((((((                   
#                  ((((((((((((((((((.       /(((((((((((((((((*                
#                /(((((((((((((((((            .(((((((((((((((((,              
#             ,((((((((((((((((((                 ((((((((((((((((((            
#           .((((((((((((((((((((                   .(((((((((((((((((          
#          (((((((((((((((((((((((                     ((((((((((((((((/        
#        (((((((((((((((((((((((((((/                    ,(((((((((((((((*      
#      .((((((((((((((/  /(((((((((((((.                   ,(((((((((((((((     
#     *((((((((((((((      ,(((((((((((((/                   *((((((((((((((.   
#    ((((((((((((((,          /(((((((((((((.                  ((((((((((((((,  
#   (((((((((((((/              ,(((((((((((((*                 ,(((((((((((((, 
#  *(((((((((((((                .(((((((((((((((                ,((((((((((((( 
#  ((((((((((((/                /((((((((((((((((((.              ,((((((((((((/
# (((((((((((((              *(((((((((((((((((((((((*             *((((((((((((
# (((((((((((((            ,(((((((((((((..(((((((((((((           *((((((((((((
# ((((((((((((,          /((((((((((((*      /((((((((((((/         ((((((((((((
# (((((((((((((        /((((((((((((/          (((((((((((((*       ((((((((((((
# (((((((((((((/     /((((((((((((               ,((((((((((((,    *((((((((((((
#  ((((((((((((((  *(((((((((((/                   *((((((((((((.  ((((((((((((/
#  *((((((((((((((((((((((((((,                      /((((((((((((((((((((((((( 
#   (((((((((((((((((((((((((                         ((((((((((((((((((((((((, 
#   .(((((((((((((((((((((((/                         ,(((((((((((((((((((((((  
#     ((((((((((((((((((((((/                         ,(((((((((((((((((((((/   
#      *(((((((((((((((((((((                         (((((((((((((((((((((,    
#       ,(((((((((((((((((((((,                      ((((((((((((((((((((/      
#         ,(((((((((((((((((((((*                  /((((((((((((((((((((        
#            ((((((((((((((((((((((,           ,/((((((((((((((((((((,          
#              ,(((((((((((((((((((((((((((((((((((((((((((((((((((             
#                 .(((((((((((((((((((((((((((((((((((((((((((((                
#                     .((((((((((((((((((((((((((((((((((((,.                   
#                          .,(((((((((((((((((((((((((.           
#     
###################################################################################
