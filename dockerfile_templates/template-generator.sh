#!/bin/bash

# for each php version
# remove existing template
# regenerate file structure
# m4 each of the 10 files - dockerfiles and others

declare -a PHP_VERSIONS=(5.3.29 5.4.45 5.5.38 5.6.37 7.0.33 7.1.33 7.2.34 7.3.33 7.4.28 7.3.x 7.4.x 8.0.28 8.0.x 8.1.16 8.2.3 8.3.0-rc1)
CONTAINERDIR=php
for PHP_LABEL in "${PHP_VERSIONS[@]}";
  do
    echo
    echo "LABEL: ${PHP_LABEL}"
    PHP_NUM=`echo ${PHP_LABEL} |awk 'BEGIN {FS="-"} {split($1,versionnums,/\./); print versionnums[1]versionnums[2]}'`
    echo "PHP_NUM: ${PHP_NUM}"
    if [ ${PHP_LABEL:4:1} = 'x' ]; then
      PHP_TYPE=branch
      PHP_VERSION=${PHP_LABEL%??}
    else
      PHP_TYPE=release
      PHP_VERSION=${PHP_LABEL}
      if ((${PHP_NUM} > 55)); then
        PHP_LABEL=${PHP_LABEL%.*}
      fi
    fi
    echo "TYPE: ${PHP_TYPE}"

    PHP_LABEL=${PHP_LABEL}-apache
    rm -rf ../${CONTAINERDIR}/${PHP_LABEL}
    # Build the dockerfile
    mkdir -p ../${CONTAINERDIR}/${PHP_LABEL}
    m4 -I . -DPHP_LABEL=${PHP_LABEL} -DPHP_TYPE=${PHP_TYPE} -DPHP_NUM=${PHP_NUM} -DPHP_VERSION=${PHP_VERSION} base.m4 > ../${CONTAINERDIR}/${PHP_LABEL}/Dockerfile

    # Copy in the docker-php-ext files
    # TODO: this could be just a copy, m4 doesnt do anything with the pecl-install file
    m4 -I . docker_includes/php/docker-php-ext-pecl-install.m4 > ../${CONTAINERDIR}/${PHP_LABEL}/docker-php-ext-pecl-install

    m4 -I . docker_includes/php/docker-php-ext-configure.m4 > ../${CONTAINERDIR}/${PHP_LABEL}/docker-php-ext-configure
    m4 -I . docker_includes/php/docker-php-ext-enable.m4 > ../${CONTAINERDIR}/${PHP_LABEL}/docker-php-ext-enable
    m4 -I . docker_includes/php/docker-php-ext-install.m4 > ../${CONTAINERDIR}/${PHP_LABEL}/docker-php-ext-install

    m4 -I . -DPHP_NUM=${PHP_NUM} docker_includes/php/docker-php-entrypoint.m4 > ../${CONTAINERDIR}/${PHP_LABEL}/docker-php-entrypoint

    m4 -I . -DTYPE=${PHP_TYPE} -DPHP_NUM=${PHP_NUM} docker_includes/php/docker-php-source.m4 > ../${CONTAINERDIR}/${PHP_LABEL}/docker-php-source

    # Generate in the php.ini files
    mkdir -p ../${CONTAINERDIR}/${PHP_LABEL}/conf/php
    m4 -I . -DPHP_NUM=${PHP_NUM} -DPHPENV_TYPE=web docker_includes/php/php.ini.m4 > ../${CONTAINERDIR}/${PHP_LABEL}/conf/php/php.ini
    m4 -I . -DPHP_NUM=${PHP_NUM} -DPHPENV_TYPE=cli docker_includes/php/php.ini.m4 > ../${CONTAINERDIR}/${PHP_LABEL}/conf/php/php-cli.ini
    # Copy in the supervisor-phantomjs.conf
    if ((${PHP_NUM} > 54)); then
      cp docker_includes/phantomjs/supervisor-phantomjs.conf ../${CONTAINERDIR}/${PHP_LABEL}/conf/supervisor-phantomjs.conf
    fi

    # Copy in the apache vhost.conf settings and build the apache2-foreground files
    mkdir -p ../${CONTAINERDIR}/${PHP_LABEL}/conf/apache2
    m4 -I . docker_includes/apache2/vhost.conf.m4 > ../${CONTAINERDIR}/${PHP_LABEL}/conf/apache2/vhost.conf
    m4 -I . docker_includes/apache2/apache2-foreground.m4 > ../${CONTAINERDIR}/${PHP_LABEL}/apache2-foreground

    chmod 755 ../${CONTAINERDIR}/${PHP_LABEL}/docker-php*
    chmod 755 ../${CONTAINERDIR}/${PHP_LABEL}/apache2-foreground*
  done

