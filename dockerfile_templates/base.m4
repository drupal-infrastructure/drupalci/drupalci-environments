######
# Base
######
ifelse(
PHP_NUM,`73',`FROM debian:buster',
PHP_NUM,`74',`FROM debian:buster',
PHP_NUM,`80',`FROM debian:buster',
PHP_NUM,`81',`FROM debian:bullseye',
PHP_NUM,`82',`FROM debian:bullseye',`FROM debian:jessie')

ENV DRUPALCI TRUE
ENV TERM xterm

include(`./docker_includes/apache2/apache.m4')
include(`./docker_includes/php/php.m4')
include(`./docker_includes/composer_drush/composer_drush.m4')
ifelse(
PHP_NUM,`54',`dnl',
PHP_NUM,`53',`dnl',
include(`./docker_includes/phantomjs/phantomjs.m4')
include(`./docker_includes/yarn_node/yarn.m4'))
ENTRYPOINT ["docker-php-entrypoint"]

WORKDIR /var/www/html

EXPOSE 80
CMD ["apache2-foreground"]
