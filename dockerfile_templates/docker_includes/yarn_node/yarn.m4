# Install node, yarn
RUN apt-get update \
    && apt-get install -y apt-transport-https ca-certificates \
    && rm -rf /var/lib/apt/lists/*
RUN curl -sS https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - \
ifelse(
PHP_NUM,`73',`    && echo "deb https://deb.nodesource.com/node_16.x buster main" | tee /etc/apt/sources.list.d/nodesource.list \',
PHP_NUM,`74',`    && echo "deb https://deb.nodesource.com/node_16.x buster main" | tee /etc/apt/sources.list.d/nodesource.list \',
PHP_NUM,`80',`    && echo "deb https://deb.nodesource.com/node_16.x buster main" | tee /etc/apt/sources.list.d/nodesource.list \',
PHP_NUM,`81',`    && echo "deb https://deb.nodesource.com/node_18.x bullseye main" | tee /etc/apt/sources.list.d/nodesource.list \',
PHP_NUM,`82',`    && echo "deb https://deb.nodesource.com/node_18.x bullseye main" | tee /etc/apt/sources.list.d/nodesource.list \',`    && echo "deb https://deb.nodesource.com/node_12.x jessie main" | tee /etc/apt/sources.list.d/nodesource.list \')
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get install -y nodejs yarn \
    && rm -rf /var/lib/apt/lists/*
