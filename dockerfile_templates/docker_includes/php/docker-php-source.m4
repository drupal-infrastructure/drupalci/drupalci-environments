define(`ARCHIVE_EXTENSION', ifelse(PHP_NUM,`54',tar.gz,tar.xz))dnl
define(`ARCHIVE_CL', ifelse(PHP_NUM,`54',tar -zxf /usr/src/php.ARCHIVE_EXTENSION,tar -Jxf /usr/src/php.ARCHIVE_EXTENSION))dnl
`#!/bin/sh
set -e

dir=/usr/src/php

usage() {
	echo "usage: $0 COMMAND"
	echo
	echo "Manage php source tarball lifecycle."
	echo
	echo "Commands:"
	echo "   extract  extract php source tarball into directory $dir if not already done."
	echo "   delete   delete extracted php source located into $dir if not already done."
	echo
}

case "$1" in
	extract)
		mkdir -p "$dir"'
ifelse(TYPE,`branch',
`		mkdir -p /tmp/phpsrc')
`		if [ ! -f "$dir/.docker-extracted" ]; then'
ifelse(TYPE,`branch',
`			unzip -q /usr/src/php.zip -d /tmp/phpsrc
			cp -r /tmp/phpsrc/*/. "$dir"
			rm -rf /tmp/phpsrc',
TYPE,`release',
`			'ARCHIVE_CL` -C "$dir" --strip-components=1',`')
`			touch "$dir/.docker-extracted"
		fi
		;;

	delete)'
ifelse(TYPE,`branch',
`		rm -rf /usr/src/php.zip',
TYPE,`release',
`		rm -rf /usr/src/php.ARCHIVE_EXTENSION',`')
`		;;

	*)
		usage
		exit 1
		;;
esac'
