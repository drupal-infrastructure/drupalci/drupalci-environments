#!/bin/bash -eux

# Upgrade the kernel to 4.9 so we get eBPF
echo "Upgrading the Kernel"
export DEBIAN_FRONTEND=noninteractive

apt-get -qq update
apt-get -qq -t jessie-backports install linux-latest-modules-4.9.0-0.bpo.6-amd64 linux-headers-4.9.0-0.bpo.6-amd64

# reboot
if [ $PACKER_BUILDER_TYPE == 'virtualbox-iso' ]; then
  echo "====> Shutting down the SSHD service and rebooting..."
  systemctl stop sshd.service
  nohup shutdown -r now < /dev/null > /dev/null 2>&1 &
  sleep 60
else
  echo "Rebooting the machine..."
  reboot
  sleep 60
fi
exit 0
